/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package numeroSeguroSocial;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Jose luis
 */
public class NumeroSeguroSocialIT {
    
    public NumeroSeguroSocialIT() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testSomeMethod() {
        NumeroSeguroSocial numeroSS=new NumeroSeguroSocial("9298808449");
        assertNotNull(numeroSS);
        Integer n=4;
        
        if(numeroSS.getNumeroDigitoVerificador()!=n)
            fail("erro numeroDigitoVerificador");
        assertTrue(numeroSS.esDigitoVerificador(4));
        assertFalse(numeroSS.esDigitoVerificador(9));
    }
    @Test(expected = IllegalArgumentException.class) 
    public void testNumeroSocialConMasCaracteres(){
           NumeroSeguroSocial numeroSS=new NumeroSeguroSocial("929880844900");
           
    }
    
    @Test(expected = IllegalArgumentException.class) 
    public void testNumeroSubdelacionIncorrecto(){
           NumeroSeguroSocial numeroSS=new NumeroSeguroSocial("0098808449");
           
    }
    
    @Test(expected = IllegalArgumentException.class) 
    public void testCreacionNumeroSeguroSocial(){
           NumeroSeguroSocial numeroSS=new NumeroSeguroSocial("9");
    }
    
    @Test 
    public void testNumeroSocialContenedordeDigitoVerificador(){
          NumeroSeguroSocial numeroSS=new NumeroSeguroSocial("92988084494");
          assertTrue(numeroSS.esDigitoVerificador(4));
          assertFalse(numeroSS.esDigitoVerificador(9));
    }
    
    @Test(expected = NumberFormatException.class)
    public void textDigitoVerificadorMayoraUndigito(){
        NumeroSeguroSocial numeroSS=new NumeroSeguroSocial("92988084494");
        numeroSS.esDigitoVerificador(-1);
    }
    
    
}

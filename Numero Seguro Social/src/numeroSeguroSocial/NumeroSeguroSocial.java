/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package numeroSeguroSocial;

import java.util.Formatter;

/**
 *
 * @author Jose luis
 */
public class NumeroSeguroSocial {
    private String numeroSocial;
    private Integer digitoVerificador;
    public NumeroSeguroSocial(String numeroSocial){
        if(numeroSocial == null|| numeroSocial.isEmpty() || numeroSocial.length()<9 || numeroSocial.length()>11 )
            throw new IllegalArgumentException("El numero esta vacio(nullo) o tiene una lonigtud diferente de 10 o 9");
        existeDigitoVerificador(numeroSocial);
        validarSubdelegacion(numeroSocial);
        this.numeroSocial=numeroSocial;
        this.digitoVerificador=calcularDigitoVerificador(numeroSocial);
        
        
    }
    private void  validarSubdelegacion(String numeroSocial){
        Integer numeroSubdelegacion=Integer.parseInt(numeroSocial.charAt(0)+""+numeroSocial.charAt(1));
        if(numeroSubdelegacion<1)
            throw new IllegalArgumentException("El numero social contiene un numero de subdelegacion incorrecto"); 
    }
    
    
    private void existeDigitoVerificador(String numeroSocial){
        if(numeroSocial.length()==11){
            Integer digitoVerificador=Integer.parseInt(String.valueOf(numeroSocial.charAt(numeroSocial.length()-1)));
            if(digitoVerificador.intValue()!=calcularDigitoVerificador(numeroSocial)){
                throw new IllegalArgumentException("EL digito Verificador contenido es incorrecto");
            }
            
        }
        
    }
    
    public boolean esDigitoVerificador(Integer numeroVerificador){
        if(numeroVerificador>9 || numeroVerificador<0)
            throw  new NumberFormatException("El numero debe contener un solo digito");
        return numeroVerificador==digitoVerificador;
    }
    
    public Integer getNumeroDigitoVerificador(){
        return digitoVerificador;
    }
    
    private Integer calcularDigitoVerificador(String numero){
        
        Integer numeroVerificador=0;
        for( Integer i=0; i< 10; i++){
            numeroVerificador += (Integer.parseInt(numero.charAt(i)+"")*((i%2)+1));
        }
        return  (numeroVerificador%10);
    }
    
    
    
    
    
}
